#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
#include <iomanip> 
#include <chrono> 
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <sstream> 
#include <exception>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include "User.h"
#include "DataBase.h"

using namespace std;
using namespace std::this_thread; 
using namespace std::chrono; 
class User;
class DataBase;
class Game
{

public:
	Game(const vector<User*> players, int questionsNo,DataBase& db);
	~Game();
	void sendFirstQuestion();
	void handleFinishGame();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int ansN, int time);
	bool leaveGame(User*);
	int getID();
	

private:
	vector<Question*> _question;
	vector<User*> _players;
	int _questions_no;
	int _currQuestionIndex;
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;
	bool insertGameToDB();
	void initQuestionsFrom();
	void sendQuestionToAllUsers();
	int _currTurn;
	int _id;
};

