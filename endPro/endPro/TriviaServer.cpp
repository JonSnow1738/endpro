
#include "TriviaServer.h"


TriviaServer::TriviaServer() :_db()
{
	//open new socket, if not ok - throw exception
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

TriviaServer::~TriviaServer()
{
	userList.clear();
	roomList.clear();

	closesocket(_socket);
}

void TriviaServer::Serve()
{
	bindAndListen();

	// create new thread for handling message
	std::thread tr(&TriviaServer::handleReacivedMessages, this);
	tr.detach();


	while (true)
	{
		// the main thread is only accepting clients 
		// and add them to the list of handlers
		cout << "accepting client..."<< endl;
		TriviaServer::acceptClient();
	}
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening (Port = 8820)...");
}

void TriviaServer::acceptClient()
{
	SOCKET client_socket = accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	cout << "Client accepted. Client Socket = " + to_string(client_socket)<<endl;
	// create new thread for client	and detach from it
	std::thread tr(&TriviaServer::clientHandler, this, client_socket);
	tr.detach();
}

void TriviaServer::clientHandler(SOCKET s)
{
	RecievedMessage* currRcvMsg = nullptr;
	try
	{
		// get the first message code
		int msgCode = Helper::getMessageTypeCode(s);

		while (msgCode != 0 || msgCode != MT_CLIENT_QUIT_BUTTON)
		{
			currRcvMsg = buildReciveMessage(s, msgCode);
			TriviaServer::addRecievedMessage(currRcvMsg);

			msgCode = Helper::getMessageTypeCode(s);
		}
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function clientHandler. socket=" << s << ", what=" << e.what() << std::endl;
		currRcvMsg = buildReciveMessage(s, MT_CLIENT_QUIT_BUTTON);
		addRecievedMessage(currRcvMsg);
	}
	closesocket(s);
}

void TriviaServer::handleReacivedMessages()
{
	int msgCode = 0;
	SOCKET clientSock = 0;
	string userName;
	
	while (true)
	{
		try
		{
			unique_lock<mutex> lck(_mtxRecievedMessages);

			// Wait for clients to enter the queue.
			if (_queRcvMessages.empty())
				_msgQueueCondition.wait(lck);

			cout << "----------------------------------------------------------------------" << endl;
			cout << "----------------------------------------------------------------------" << endl;

			// in case the queue is empty.
			if (_queRcvMessages.empty())
				continue;

			RecievedMessage* currMessage = _queRcvMessages.front();
			_queRcvMessages.pop();
			lck.unlock();
		
			// Extract the data from the tuple.
			clientSock = currMessage->getSock();
			msgCode = currMessage->getMessageCode();
			cout << "handleRecievedMessages: msgCode = " + to_string(msgCode) + ", client_socket: " + to_string(clientSock) << endl;

			if (msgCode == MT_CLIENT_LOG_IN)
			{
				cout << "Adding new user to connected users list : socket = " + to_string(clientSock )+ ", username = " + _currUser << endl;
				handleSignin(currMessage);
				Helper::sendData(clientSock, _currMsg);

			}
			else if (msgCode == MT_CLIENT_SIGN_OUT)
			{
				cout << "Erase user from connected users list: username = " + _currUser + ", socket = " + to_string(clientSock) << endl;
				handleSignout(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_SIGN_UP)
			{
				handleSignup(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_GET_ROOMS)
			{
				handleGetRooms(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_GET_USERS)
			{
				handleGetUsersInRoom(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_JOIN_ROOM)
			{
				bool joined = handleJoinRoom(currMessage);

				Helper::sendData(clientSock, _currMsg);

				if (joined == true)
				{
					User* currU = getUserByName(_currUser);
					for (int i = 0; i < currU->getRoom()->getUsers().size(); i++){
						currU->getRoom()->getUsers().at(i)->send(currU->getRoom()->getUsersListMessage());
					}
				}
			}
			else if (msgCode == MT_CLIENT_LEAVE_ROOM)
			{
				bool lr = handleLeaveRoom(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_CREATE_ROOM)
			{
				bool g = handleCreateRoom(currMessage);
			}
			else if (msgCode == MT_CLIENT_CLOSE_ROOM)
			{
				bool cr = handleCloseRoom(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_START_GAME)
			{
				handleStartGame(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_GET_ANSWER)
			{
				handlePlayerAnswer(currMessage);
				Helper::sendData(clientSock, _currMsg);

				
			}
			else if (msgCode == MT_CLIENT_LEAVE_GAME)
			{
				handleLeaveGame(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_BEST_SCORES)
			{
				handleGetBestScores(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_PERSONAL_STATUS)
			{
				handleGetPersonalStatues(currMessage);
				Helper::sendData(clientSock, _currMsg);
			}
			else if (msgCode == MT_CLIENT_QUIT_BUTTON)
			{
				safeDeleteUser(currMessage);
			}
			if (msgCode != MT_CLIENT_QUIT_BUTTON){
				cout << "Message sent to user : " + _currUser + ", msg : " + _currMsg << endl;
			}
			delete currMessage;
			cout << "----------------------------------------------------------------------" << endl;
			cout << "----------------------------------------------------------------------" << endl;
		}
		catch (...)
		{

		}
	}

}

void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	unique_lock<mutex> lck(_mtxRecievedMessages);

	_queRcvMessages.push(msg);
	lck.unlock();
	_msgQueueCondition.notify_all();

}

RecievedMessage* TriviaServer::buildReciveMessage(SOCKET client_socket, int msgCode)
{
	RecievedMessage* msg = nullptr;
	vector<string> values;
	char* data;
	if (msgCode == MT_CLIENT_LOG_IN)
	{
		data = Helper::getPartFromSocket(client_socket, msgCode,0);
		parseData(data);
	}
	else if (msgCode == MT_CLIENT_SIGN_UP)
	{

		data = Helper::getPartFromSocket(client_socket, msgCode, 0);
		parseDataSingUp(data);
	}
	else if (msgCode == MT_CLIENT_GET_USERS)
	{
		data = Helper::getPartFromSocket(client_socket, msgCode, 0);
		parseRoomId(data);
	}
	else if (msgCode == MT_CLIENT_JOIN_ROOM)
	{
		data = Helper::getPartFromSocket(client_socket, msgCode, 0);
		parseRoomId(data);
	}
	else if (msgCode == MT_CLIENT_CREATE_ROOM)
	{

		data = Helper::getPartFromSocket(client_socket, msgCode, 0);
		parseCroom(data);
		
	}
	else if (msgCode == MT_CLIENT_JOIN_ROOM)
	{
		data = Helper::getPartFromSocket(client_socket, msgCode, 0);
		parseRoomId(data);
	}
	else if (msgCode == MT_CLIENT_GET_ANSWER)
	{
		data = Helper::getPartFromSocket(client_socket, msgCode, 0);
		parsePanswer(data);
	}
	msg = new RecievedMessage(client_socket, msgCode, values);
	msg->setUser(getUserByName(_currUser));
	return msg;

}

void TriviaServer::safeDeleteUser(RecievedMessage* msg)
{
	try
	{
		SOCKET socket = msg->getSock();
		string sock = to_string(socket);
		handleSignout(msg);
		cout << "Closing socket = " + sock << endl;
		closesocket(socket);
		cout << "User has disconnected : socket = " + sock << endl;

	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in safeDeleteUser: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in safeDeleteUser !" << std::endl;
	}
}

void TriviaServer::handleSignout(RecievedMessage* msg)
{
	//might need to change the erase part.
	SOCKET socket = msg->getSock();
	
	if (msg->getUser() == NULL)
	{
		return;
	}

	User* user = msg->getUser();
	std::map<SOCKET,User*>::iterator it;

	it = userList.find(socket);

	if (it != userList.end() && it->second == user)
	{
		cout << "Erase user from connected users list : username = "+_currUser+", socket = "+to_string(it->second->getSocket()) << endl;
		userList.erase(it);
	}
}


User* TriviaServer::handleSignin(RecievedMessage* msg)
{
	User* currU;
	bool check = _db.DataBase::isUserAndPassMatch(_currUser,_currPass);
	SOCKET sk = msg->getSock();
	_currMsg = "1020";
	
	if (check == false){
		_currMsg = "1021";
		cout << "SEND: User signed in faild : username = " + _currUser << endl;
		return nullptr;
	}
	
	if (getUserByName(_currUser)){
		_currMsg = "1022";
		cout << "SEND: User signed in faild : username = " + _currUser << endl;
		return nullptr;
		
	}
	
	currU = new User(_currUser, msg->getSock());

	userList.insert(pair<SOCKET, User*>(msg->getSock(), currU));
	msg->setUser(currU);
	cout << "SEND: User signed in successfuly : username = " + _currUser + ", socket = " + to_string(currU->getSocket()) << endl;
	return currU;
}

User* TriviaServer::getUserByName(string user)
{
	std::map<SOCKET, User*>::iterator it;
	
	for (it = userList.begin(); it != userList.end(); ++it)
		if (it->second->getUserName() == user)
			return it->second;
	return nullptr;
}


bool TriviaServer::handleSignup(RecievedMessage* msg){
	User* currU = msg->getUser();
	if (Validator::isPasswordValid(_currPass)){
		if (Validator::isUserNameValid(_currUser)){
			if (!_db.isUserExists(_currUser)){
				if (_db.addNewUser(_currUser, _currPass, _currMail)){
					_currMsg = "1040";
					User* user = new User(_currUser, _socket);
					currU = user;
					cout << "SEND: User signup Succeeded. username = " + _currUser + ", socket = " + to_string(_socket) << endl;
					return true;
				}
				else{
					_currMsg = "1044";
				}
			}
			else{
				_currMsg = "1042";
			}
		}
		else{
			_currMsg = "1043";
		}
	}
	else{
		_currMsg = "1041";
	}
	cout << "SEND: User signup failed. username = "+_currUser+", socket = "+to_string(currU->getSocket()) << endl;
	return false;
}

void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{
	User* currU = msg->getUser();

	if (currU->leaveGame() == true)
	{
		delete(currU->getGame());
	}
}

//todo
void TriviaServer::handleStartGame(RecievedMessage* msg)
{
	try
	{
		std::map<int, Room*>::iterator it;


		User* currU = msg->getUser();
		Game* game = new Game(currU->getRoom()->getUsers(), currU->getRoom()->getQuestionsNo(), _db);
		if (game != nullptr){
			it = roomList.find(currU->getRoom()->getId());
			_db.gameAm(currU->getRoom()->getUsers());
			if (it != roomList.end())
				roomList.erase(it);

			currU->setGame(game);
			
			currU->getGame()->sendFirstQuestion();
		}
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in handleStartGame: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in handleStartGame !" << std::endl;
	}
}
void TriviaServer::handlePlayerAnswer(RecievedMessage* msg)
{
	User* currU = msg->getUser();
	Game* game = currU->getGame();
	if (game != NULL)
	{	
		bool check = game->handleAnswerFromUser(currU, _ansNum, _ansTime);
		_db.updateAveTime(currU,_ansTime);
		_currMsg = currU->getCurrMsg();
		
		if (check == false)
		{
			delete(currU->getGame());
		}
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	User* currU = msg->getUser();
	if (currU == nullptr )
	{
		cout << "create Room was faild" << endl;
		_currMsg = "1141";
		return false;
	}

	if (currU->createRoom(_roomId, _roomName, _playerNumber, _questionNumber, _questionTime))
	{
		_roomIdSequence++;

		roomList.insert(pair<int, Room*>(_roomId, currU->getRoom()));
		_currMsg = "1140";
		cout << "Room was created : roomId = " + to_string(_roomId) + ", roomName = " + _roomName + ", adminName = " + _currUser + ", playersNo = " + to_string(_playerNumber) + ", QuestionsNo = " + to_string(_questionNumber) + ", AnswerTime = " + to_string(_questionTime) << endl;
		
		return true;
	}
	cout << "create Room was faild" << endl;
	_currMsg =  "1141";
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	Room* currRoom = msg->getUser()->getRoom();
	if (currRoom == nullptr)
	{
		cout << "Room was not closed" << endl;
		return false;
	}
	int roomId = msg->getUser()->closeRoom();
	if (roomId != -1)
	{
		std::map<int, Room*>::iterator it;

		for (it = roomList.begin(); it != roomList.end(); ++it)
		{
			if (it->second == currRoom)
			{
				cout << "Room was closed : roomId = " + to_string(it->second->getId()) + ", roomName = " + it->second->getName()+ ", closingUser = " + _currUser << endl;
				roomList.erase(it);
				break;
			}
		}
		
		_currMsg = "116";
		
		return true;
	}
	cout << "Room was not closed" << endl;
	return false;
}
bool TriviaServer::handleJoinRoom(RecievedMessage* msg)
{
	User* currU = msg->getUser();

	if (currU == nullptr)
	{
		_currMsg = "1102";
		return false;
	}
	Room* room = getRoomById(_roomID);
	if (room == nullptr)
	{
		_currMsg = "1102";
		return false;
	}

	bool joined = currU->joinRoom(room);
	if (joined == true)
	{
		_currMsg = "1100";
		
		if (_questionNumber < 10)
			_currMsg += "0";
			
		_currMsg += to_string(_questionNumber);
		
		if (_questionTime < 10)
			_currMsg += "0";
			
		_currMsg += to_string(_questionTime);
	}

	return joined;
}
bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	User* currU = msg->getUser();

	if (currU == nullptr){
		_currMsg = "116";
		return false;
	}
	else if (currU->getRoom() == nullptr){
		_currMsg = "116";
		return false;
	}
	currU->leaveRoom();
	_currMsg = "1120";
	return true;
}
void TriviaServer::handleGetUsersInRoom(RecievedMessage* msg)
{
	User* currU = msg->getUser();
	int roomId = _roomID;
	Room* rm = getRoomById(_roomID);

	if (rm == nullptr){
		_currMsg = "1080";
		return;
	}
	_currMsg = rm->getUsersListMessage();

}

User* TriviaServer::getUserBySocket(SOCKET user){
	std::map<SOCKET, User*>::iterator it;

	it = userList.find(user);
	
	return it->second;
}

void TriviaServer::handleGetRooms(RecievedMessage* msg)
{
	User* currU = msg->getUser();
	string message = "106";
	int rooms;
	rooms = roomList.size();
	if (rooms == 0)
	{
		_currMsg = "1060000";
		return;
	}
	if (rooms < 10)
	{
		message += "000";
	}
	else if (rooms < 100)
	{
		message += "00";
	} 
	else
	{
		message += "0";
	}
	message += to_string(rooms);
	
	int i, roomId, roomNameSize;
	map<int, Room*>::iterator it;

	for (i = 0; i < roomList.size(); i++){
		it = roomList.find(i+1);
		roomId = it->second->getId();

		if (roomId < 10)
		{
			message += "000";
		}
		else if (roomId < 100)
		{
			message += "00";
		}
		else
		{
			message += "0";
		}

		message += to_string(roomId);

		roomNameSize = it->second->getName().size();
		if (roomNameSize < 10){
			message += "0";
		}
		message += to_string(roomNameSize);
		message += it->second->getName();
	}

	_currMsg = message;
	cout << "Message sent to user : " + _currUser + ", msg : " + message << endl;
}

void TriviaServer::handleGetBestScores(RecievedMessage* msg)
{
	vector<string> bestScores = _db.getBestScores();
	User* currU = msg->getUser();
	string message = "124";
	//checks if thers three best scores
	for (int i = 0; i < 6; i++){
		if (bestScores.at(i).length() < 10){
			message += "0";
		}
		message += to_string(bestScores.at(i).length());
		message += bestScores.at(i);
		i++;
		for (int j = bestScores.at(i).size(); j< 6; j++){
			message += "0";
		}
		message += bestScores.at(i);
	}
	_currMsg = message;
	cout << "Message sent to user : " + _currUser + ", msg : " + _currMsg << endl;
}
void TriviaServer::handleGetPersonalStatues(RecievedMessage* msg)
{
	User* currU = msg->getUser();
	vector<string> ps = _db.getPersonalStatus(currU->getUserName());
	string message = "126";

	int numOfGames, numOfRights, numOfWrongs;
	float avgTime;
	numOfGames = stoi(ps.at(0));
	numOfRights = stoi(ps.at(1));
	numOfWrongs = stoi(ps.at(2));
	avgTime = stof(ps.at(3));
	string avg,time;
	time = to_string(avgTime);
	if (avgTime < 10)
	{
		avg = "0";
	}

	avg += to_string(time.at(0));
	avg += to_string(time.at(2));
	avg += to_string(time.at(3));

	if (numOfGames == 0 && numOfRights == 0 && numOfWrongs == 0)
	{
		_currMsg = "1260000";
		return;
	}
	
	
	if (numOfGames < 10)
	{
		message += "000";
	}
	else if (numOfGames < 100)
	{
		message += "00";
	}
	else 
	{
		message += "0";
	}
	message += to_string(numOfGames);

	if (numOfRights < 10)
	{
		message += "00000";
	}
	else if (numOfRights < 100)
	{
		message += "0000";
	}
	else if (numOfRights < 1000)
	{
		message += "000";
	}	
	else if (numOfRights < 10000)
	{
		message += "00";
	}
	else
	{
		message += "0";
	}
	message += to_string(numOfRights);


	if (numOfWrongs < 10)
	{
		message += "00000";
	}
	else if (numOfWrongs < 100)
	{
		message += "0000";
	}
	else if (numOfWrongs < 1000)
	{
		message += "000";
	}
	else if (numOfWrongs < 10000)
	{
		message += "00";
	}
	else
	{
		message += "0";
	}
	message += to_string(numOfWrongs);
	
	message += avg;	

	_currMsg = message;
}

void TriviaServer::parseRoomId(char* data)
{
	_roomID = stoi(to_string(data[0])) + stoi(to_string(data[1])) + stoi(to_string(data[2])) + stoi(to_string(data[3])) - 192;
}

Room* TriviaServer::getRoomById(int roomId){
	map<int, Room*>::iterator it;

	it = roomList.find(roomId);

	return it->second;
}

void TriviaServer::parseCroom(char* data)
{
	int i;
	_roomId = _roomIdSequence + 1;
	int nameLen = stoi(to_string(data[0])) + stoi(to_string(data[1])) - 96;
	
	for (i = 0; i < nameLen; i++)
	{
		_roomName += data[i+2];
	}

	_playerNumber = data[nameLen + 2] - 48;
	
	string qn = to_string(data[nameLen + 3] - 48);
	qn+= to_string(data[nameLen + 4]-48);

	_questionNumber = atoi(qn.c_str());

	qn = to_string(data[nameLen + 5] - 48);
	qn += to_string(data[nameLen + 6] - 48);
	_questionTime = atoi(qn.c_str());
}


void TriviaServer::parseData(char* data){
	int userLen, passLen, i;
	string user, password;

	if (data[0] == '0')
	{
		userLen = data[1] - 48;
	}
	else
	{
		userLen = (data[0] - 48) * 10;
		userLen += data[1] - 48;
	}

	for (i = 0; i < userLen; i++)
	{
		user += data[i + 2];
	}

	_currUser = user;

	if (data[2 + userLen] == '0')
	{
		passLen = data[3 + userLen] - 48;
	}
	else
	{
		passLen = (data[2 + userLen] - 48) * 10;
		passLen += data[3 + userLen] - 48;
	}

	for (i = 0; i < passLen; i++)
	{
		password += data[i + userLen + 4];
	}

	_currPass = password;
}

void TriviaServer::parseDataSingUp(char* data){
	int userLen, passLen, i, mailLen;
	string user, password, mail;

	if (data[0] == '0')
	{
		userLen = data[1] - 48;
	}
	else
	{
		userLen = (data[0] - 48) * 10;
		userLen += data[1] - 48;
	}

	for (i = 0; i < userLen; i++)
	{
		user += data[i + 2];
	}

	_currUser = user;

	if (data[2 + userLen] == '0')
	{
		passLen = data[3 + userLen] - 48;
	}
	else
	{
		passLen = (data[2 + userLen] - 48) * 10;
		passLen += data[3 + userLen] - 48;
	}

	for (i = 0; i < passLen; i++)
	{
		password += data[i + userLen + 4];
	}

	_currPass = password;

	if (data[4 + userLen + passLen] == '0')
	{
		mailLen = data[9 + userLen + passLen] - 48;
	}
	else
	{
		mailLen = (data[8 + userLen] - 48) * 10;
		mailLen += data[9 + userLen] - 48;
	}

	for (i = 0; i < mailLen; i++)
	{
		mail += data[i + userLen + passLen + 6];
	}

	_currMail = mail;
}

void TriviaServer::parsePanswer(char* data)
{
	_ansNum = data[0] - 48;
	_ansTime = stoi(to_string(data[1])) + stoi(to_string(data[2])) - 96;
}

void TriviaServer::parseBS(char* data){
	_currMsg = "223";
}
