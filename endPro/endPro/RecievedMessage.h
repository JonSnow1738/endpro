#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <iomanip> 
#include <chrono> 
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <exception>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>
#include "User.h"

using namespace std;

class User;
class RecievedMessage
{
public:
	RecievedMessage(SOCKET sk, int messageCode);
	RecievedMessage(SOCKET sk, int messageCode, vector<string> values);
	SOCKET getSock();
	User* getUser();
	void setUser(User* user);
	int getMessageCode();
	vector<string>& getValues();

private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
};