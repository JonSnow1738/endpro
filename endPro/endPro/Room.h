#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <iomanip> 
#include <chrono> 
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <exception>
#include <queue>
#include <map>
#include <mutex>

#include "User.h"



using namespace std;

class User;
class Room
{
public:
	Room(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime, User* admin);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuestionsNo();
	int getId();
	User* getAdmin();
	string getName();
	int getTime();
	void setFull(bool a);

private:
	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
	bool _full;


	string getUsersAsString(vector<User*> users, User* user);
	void sendMessage(string msg);
	void sendMessage(User* user, string msg);
};