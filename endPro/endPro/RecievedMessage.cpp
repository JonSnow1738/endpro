#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET sk, int messageCode)
{
	_sock = sk;
	_messageCode = messageCode;
}

RecievedMessage::RecievedMessage(SOCKET sk, int messageCode, vector<string> values){
	_sock = sk;
	_messageCode = messageCode;
	_values = values;
}

int RecievedMessage::getMessageCode()
{
	return _messageCode;
}

SOCKET RecievedMessage::getSock()
{
	return _sock;
}

User* RecievedMessage::getUser(){
	return _user;
}

void RecievedMessage::setUser(User* user){
	_user = user;
}

vector<string>& RecievedMessage::getValues(){
	return _values;
}