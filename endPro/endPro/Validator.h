#pragma once
#include <string>
using namespace std;

static class Validator
{
public:
	static bool isPasswordValid(string password);
	static bool isUserNameValid(string userName);
};