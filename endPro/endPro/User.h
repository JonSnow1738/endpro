#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <iomanip> 
#include <chrono> 
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <exception>
#include <queue>
#include <map>
#include <mutex>

#include "RecievedMessage.h"
#include "helper.h"
#include "Room.h"
#include "Game.h"
using namespace std;

class Room;
class RecievedMessage;
class Game;
class User
{
	public:
		User(string name, SOCKET sk);
		void send(string msg);
		string getUserName();
		SOCKET getSocket();
		Room* getRoom();
		Game* getGame();
		void setGame(Game* G);
		void clearRoom();
		bool createRoom(int id,string name, int maxU, int questionNo, int questiontime);
		bool joinRoom(Room* rm);
		int closeRoom();
		void leaveRoom();
		bool leaveGame();
		void setCurrMsg(string Msg);
		string getCurrMsg();
		bool getReady();
		void setSocket(SOCKET sock);
		void setReady(bool ready);
	private:
		string _username;
		Room* _currRoom;
		Game* _currGame;
		string _currMsg;
		SOCKET _sock;
		bool _ready;
};