#pragma once

#include <vector>
#include <string>
#include <WinSock2.h>

using namespace std;

enum MessageType : byte
{
	MT_CLIENT_LOG_IN = 200,
	MT_CLIENT_SIGN_OUT = 201,
	MT_SERVER_SIGN_IN = 102,
	MT_CLIENT_SIGN_UP = 203,
	MT_SERVER_SING_UP = 104,
	MT_CLIENT_GET_ROOMS = 205,
	MT_SERVER_SEND_ROOMS = 106,
	MT_CLIENT_GET_USERS = 207,
	MT_SERVER_SEND_USERS_IN_ROOM = 108,
	MT_CLIENT_JOIN_ROOM = 209,
	MT_SERVER_JOIN_ROOM = 110,
	MT_CLIENT_LEAVE_ROOM = 211,
	MT_SERVER_LEAVE_ROOM = 112,
	MT_CLIENT_CREATE_ROOM = 213,
	MT_SERVER_CREATE_ROOM = 114,
	MT_CLIENT_CLOSE_ROOM = 215,
	MT_SERVER_CLOSE_ROOM = 116,
	MT_CLIENT_START_GAME = 217,
	MT_SERVER_SEND_QUESTION = 118,
	MT_CLIENT_GET_ANSWER = 219,
	MT_SERVER_CHECK_ANSWER = 120,
	MT_SERVER_END_GAME = 121,
	MT_CLIENT_LEAVE_GAME = 222,
	MT_CLIENT_BEST_SCORES = 223,
	MT_SERVER_BEST_SCORES = 124,
	MT_CLIENT_PERSONAL_STATUS = 225,
	MT_SERVER_PERSONAL_STATUS = 126,
	MT_CLIENT_QUIT_BUTTON = 299,
};


class Helper
{
public:

	 
	 
	 static int getMessageTypeCode(SOCKET sc);
	 static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);
	 static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	 static string getStringPartFromSocket(SOCKET sc, int bytesNum);
	 static void sendData(SOCKET sc, std::string message);
	 static string getPaddedNumber(int num, int digits);


private:
	static char* Helper::getPartFromSocket(SOCKET sc, int bytesNum);

};


#ifdef _DEBUG // vs add this define in debug mode
#include <stdio.h>
// Q: why do we need traces ?
// A: traces are a nice and easy way to detect bugs without even debugging
// or to understand what happened in case we miss the bug in the first time
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
// for convenient reasons we did the traces in stdout
// at general we would do this in the error stream like that
// #define TRACE(msg, ...) fprintf(stderr, msg "\n", __VA_ARGS__);

#else // we want nothing to be printed in release version
#define TRACE(msg, ...) printf(msg "\n", __VA_ARGS__);
#define TRACE(msg, ...) // do nothing
#endif