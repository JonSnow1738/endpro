#include "Question.h"

Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4) : _question(question)
{
	int i,flag1 = 0 , flag2 = 0 , flag3 = 0, flag4 = 0;
	int iSecret;
	_id = id;
	srand(time(NULL));
	string answers[5] = {""};


	while (answers[4] == "" || answers[1] == "" || answers[2] == "" || answers[3] == ""){

		iSecret = rand() % 4 + 1;
		if (answers[iSecret] == "" && flag1 == 0){
			answers[iSecret] = correctAnswer;
			_correctAnswerIndex = iSecret;
			flag1 = 1;
		}

		iSecret = rand() % 4 + 1;
		if (answers[iSecret] == "" && flag2 == 0){
			answers[iSecret] = answer2;
			flag2 = 1;
		}

		iSecret = rand() % 4 + 1;
		if (answers[iSecret] == "" && flag3 == 0){
			answers[iSecret] = answer3;
			flag3 = 1;
		}

		iSecret = rand() % 4 + 1;
		if (answers[iSecret] == "" && flag4 == 0){
			answers[iSecret] = answer4;
			flag4 = 1;
		}
	}
	for (int i = 0; i < 4; i++){
		_answers[i] = answers[i + 1];
	}
	
}

string Question::getQuestion()
{
	return _question;
}
string* Question::getAnswers()
{
	return _answers;
}
int Question::getCorrectAnswerIndex()
{
	return _correctAnswerIndex;
}
int Question::getId()
{
	return _id;
}