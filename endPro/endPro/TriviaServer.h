#pragma comment(lib, "Ws2_32.lib")
#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <iomanip> 
#include <chrono> 
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <exception>
#include <queue>
#include <map>
#include <mutex>
#include <condition_variable>

#include "Validator.h"
#include "Helper.h"
#include "RecievedMessage.h"
#include <exception>
#include "Room.h"
#include "User.h"
#include "DataBase.h"

static const unsigned short PORT = 8820;
static const unsigned int IFACE = 0;

using namespace std;

class DataBase;
class Room;
class User;
class TriviaServer
{
	public:
		TriviaServer();
		~TriviaServer();
		void Serve();
		
	private:

		void bindAndListen();
		void acceptClient();
		void clientHandler(SOCKET s);

		void handleReacivedMessages();
		void addRecievedMessage(RecievedMessage* msg);
	    RecievedMessage* buildReciveMessage(SOCKET sk, int mssg);
		

		void safeDeleteUser(RecievedMessage* msg);
		void handleSignout(RecievedMessage* msg);
		bool handleSignup(RecievedMessage* msg);
		User* handleSignin(RecievedMessage* msg);


		void handleLeaveGame(RecievedMessage* msg);
		void handleStartGame(RecievedMessage* msg);
		void handlePlayerAnswer(RecievedMessage* msg);

		bool handleCreateRoom(RecievedMessage* msg);
		bool handleCloseRoom(RecievedMessage* msg);
		bool handleJoinRoom(RecievedMessage* msg);
		bool handleLeaveRoom(RecievedMessage* msg);
		void handleGetUsersInRoom(RecievedMessage* msg);
		void handleGetRooms(RecievedMessage* msg);

		void handleGetBestScores(RecievedMessage* msg);
		void handleGetPersonalStatues(RecievedMessage* msg);

		void parseCroom(char* data);
		void parseData(char* data);
		void parseDataSingUp(char* data);
		void parseRoomId(char* data);
		void parsePanswer(char* data);
		void parseBS(char* data);
		void parseGetAnswer(char* data);

		User* getUserByName(string user);
		User* getUserBySocket(SOCKET user);
		Room* getRoomById(int room);
		
		SOCKET _socket;
		map<SOCKET,User*> userList;
		DataBase _db;
		map < int, Room*  > roomList;
		condition_variable _msgQueueCondition;
		mutex _mtxRecievedMessages;
		queue<RecievedMessage*> _queRcvMessages;

		int _roomIdSequence;
		string _currPass;
		string _currUser;
		string _currMail;
		string _currMsg;
		
		int _roomId;
		int _playerNumber;
		string _roomName;
		int _questionNumber;
		int _questionTime;
		int _answerNo;
		int _timeAnswer;

		int _roomID;

		int _ansNum;
		int _ansTime;
};