#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <time.h>
#include <iomanip> 
#include <chrono> 
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <exception>
#include <queue>
#include <map>
#include <mutex>
#include "sqlite3.h"
#include <unordered_map>

using namespace std;


class Question
{

public:
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();

private:
	string _question;
	string _answers[4];
	int _correctAnswerIndex;
	int _id;
}; 