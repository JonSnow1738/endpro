#include "User.h"


User::User(string name, SOCKET sk) : _username(name)
{
	_ready = false;
	_sock = sk;
}

string User::getUserName(){
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

void User::send(string msg)
{
	Helper::sendData(_sock, msg);
}

void User::setSocket(SOCKET sock){
	_sock = sock;
}

Room* User::getRoom(){
	return _currRoom;
}

Game* User::getGame(){
 	return _currGame;
}

void User::setGame(Game* G){
	_currGame = G;
}
void User::clearRoom(){
	_currRoom = nullptr;
}
bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime){
	Room* room = new Room(roomId, roomName, maxUsers, questionsNo, questionTime,this);
	send("1140"); 
	room->setFull(room->joinRoom(this));


	if (room != nullptr)
	{
		_currRoom = room;
		return true;
	}
	return false;
}

void User::setCurrMsg(string Msg){
	_currMsg = Msg;
}

string User::getCurrMsg(){
	return _currMsg;
}
bool User::joinRoom(Room* rm)
{
	if (_currRoom == nullptr)
	{
		if (!rm->joinRoom(this))
		{
			return false;
		}
	
		_currRoom = rm;

		return true;
	}
	return false;
}
int User::closeRoom()
{
	if (this->getRoom() == NULL)
	{
		return -1;
	}
	int room_id = _currRoom->closeRoom(this);

	if (room_id == -1)
	{
		return -1;
	}

	clearRoom();
	return room_id;
}

bool User::leaveGame()
{
	if (_currGame == nullptr)
	{
		return true;
	}

	_currGame = nullptr;
	return true;
}

bool User::getReady()
{
	return _ready;
}
void User::setReady(bool ready)
{
	_ready = ready;
}

void User::leaveRoom(){
	if (_currRoom != NULL){
		this->getRoom()->leaveRoom(this);
		_currRoom = nullptr;
	}
}