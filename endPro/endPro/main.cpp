#include "TriviaServer.h"
#include "WSAInitializer.h"
#include <unordered_map>
//todo - status,fix multiplayer + best scores, why after game get 120.
unordered_map<string, vector<string>> results;
int callback(void* notUsed, int argc, char** argv, char** azCol);
int main()
{
	
	try
	{
	
		TRACE("Starting...");
		WSAInitializer wsa_init;
		TriviaServer moshe;
		
		moshe.Serve();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
	
	system("PAUSE");
	return 0;

}


int callback(void* notUsed, int argc, char** argv, char** azCol){
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}