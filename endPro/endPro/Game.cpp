#include "Game.h"


Game::Game(const vector<User*> players, int questionsNo, DataBase& db) : _db(db)
{
	_db = db;
	_players = players;
	_currTurn = 0;
	_id = _db.insertNewGame();
	if (_id == 0){
		throw ERROR;
	}
	_questions_no = 1;
	_results.clear();
	_question = _db.initQuestion(questionsNo);
	for (int j = 0; j < _players.size(); j++)
	{
		_players.at(j)->setGame(this);
		_results.insert(std::pair<string, int>(_players.at(j)->getUserName(), 0));
	}
}

Game::~Game()
{
	_question.clear();
	_players.clear();
}
//more checks date 17.10
void Game::sendQuestionToAllUsers()
{
	_currentTurnAnswers = 0;
	string message = "118";
	string *ans = _question.at(_questions_no - 1)->getAnswers();
	string ans1, ans2, ans3, ans4;
	int i;


	string quest = _question.at(_questions_no-1)->getQuestion();
	int quest_len = quest.length();

	//sleep_until(system_clock::now() + seconds(3));
	_currQuestionIndex = _question.at(_questions_no- 1)->getCorrectAnswerIndex();
	if (quest_len == 0)
	{
		string adMsg = "1180";
		_players.at(0)->getRoom()->getAdmin()->send(adMsg);
	}

	if (quest_len < 99){
		if (quest_len < 10){
			message += '0'; 
		}
		message += '0' + to_string(quest_len);
	}
	message += quest;
	//get the answers + answers Length.
	
	
	ans1 = ans[0];
	int ans1Len = ans1.length();
	if (ans1Len < 99){
		if (ans1Len < 9){ message += '0'; }
		message += '0' + to_string(ans1Len);
	}
	message += ans1;

	ans2 = ans[1];
	int ans2Len = ans2.length();
	if (ans2Len < 99){
		if (ans2Len < 9){ message += '0'; }
		message += '0' + to_string(ans2Len);
	}
	message += ans2;

	ans3 = ans[2];
	int ans3Len = ans3.length();
	if (ans3Len < 99){
		if (ans3Len < 9){ message += '0'; }
		message += '0' + to_string(ans3Len);
	}
	message += ans3;

	ans4 = ans[3];
	int ans4Len = ans4.length();
	if (ans4Len < 99){
		if (ans4Len < 9){ message += '0'; }
		message += '0' + to_string(ans4Len);
	}
	message += ans4;

	
	for(int i = 0; i < _players.size(); i++)
	{
		try
		{
			_players.at(i)->send(message);
 		}

		catch (exception e)
		{
			cout << e.what() << " Error at " << _players.at(i)->getUserName() << endl;
		}
	}
	
	
}

void Game::sendFirstQuestion()
{
	sendQuestionToAllUsers();
}

void Game::handleFinishGame()
{
	_db.updateGameStatus(getID());
	string message = "121";
	message += to_string(_players.size());
	int userLen;
	string userName;
	map<string , int>::iterator it;

	for (int i = 0; i < _players.size(); i++)
	{
		userName = _players.at(i)->getUserName();
		userLen = userName.length();

		it = _results.find(userName);

		if (userLen < 10){
			message += '0';
		}
		message += to_string(userLen);
		message += userName;
		message += to_string(it->second);

		try
		{
			_players.at(i)->setCurrMsg(message);
		}

		catch (exception e)
		{
			cout << e.what() << " Error at " << _players.at(i)->getUserName() << endl;
		}
		_players.at(i)->setGame(nullptr);
	}

}

bool Game::handleNextTurn()
{
	int i,j=0;
	if (_players.empty())
	{
		handleFinishGame();
		return false;
	}
	
	while (j != _players.size()){
		for (i = 0; i < _players.size(); i++)
		{
			if (_players.at(i)->getReady() == true){
				j++;
			}
		}
	}
	//todo
	sleep_until(system_clock::now() + seconds(1));

	
	if (j == _players.size())
	{
		if (_questions_no == _question.size())
		{
			
			handleFinishGame();
			return false;
		}
		else
		{
			_questions_no++;
			sendQuestionToAllUsers();
			return true;
		}
	}

	return false;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	bool right = false;
	_currentTurnAnswers += 1;
	user->setReady(true);
	
	if (answerNo == _currQuestionIndex)
	{
		_results.at(user->getUserName()) += 1;
		right = true;
	}
	if (right){
		user->setCurrMsg("1201");
	}
	else{
		user->setCurrMsg("1200");
	}

	
	string *ans = _question.at(_currentTurnAnswers-1)->getAnswers();
	string ans1;
	ans1 = ans[answerNo - 1];
	if (answerNo == 5)
	{
		ans1 = "";
	}
	
	bool add = _db.addAnswerToPlayer(getID(), user->getUserName(), _question.at(_currQuestionIndex-1)->getId(), ans1, right, time);
	
	
	return handleNextTurn();
}

bool Game::leaveGame(User* currUser)
{
	vector <User*>::iterator it;
	int i;
	for (i=0; i < _players.size(); i++){
		if (_players.at(i) == currUser){
			_players.erase(it);
			return handleNextTurn();
		}
		it++;
	}
	return false;
}

int Game::getID()
{
	return _id;
}