#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <iomanip> 
#include <chrono> 
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <Windows.h>
#include <exception>
#include <queue>
#include <map>
#include <mutex>
#include "sqlite3.h"
#include "Question.h"
#include <unordered_map>
#include "User.h"

using namespace std;
using namespace std::this_thread;
using namespace std::chrono;


class DataBase 
{

public:
	DataBase();
	~DataBase();
	DataBase& operator=(const DataBase& other);
	bool isUserExists(string name);
	bool addNewUser(string user, string pass, string mail);
	bool isUserAndPassMatch(string user, string pass);
	vector<Question*> initQuestion(int num);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string user);
	int insertNewGame();
	bool updateGameStatus(int id);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
	vector<int> bubbleSort(vector<int> values);
	void gameAm(vector<User*> users);
	void updateAveTime(User* user, int ansTime);
private:
	static int callback(void* notUsed, int argc, char** argv, char** azCol);

	static int callbackCount(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackQuestions(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackBestScores(void* notUsed, int argc, char** argv, char** azCol);
	static int callbackPersonalStatus(void* notUsed, int argc, char** argv, char** azCol);
	sqlite3 *_db;
	int _rc;
	int questionAm;
	char *_zErrMsg;

};

