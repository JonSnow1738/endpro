#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	bool space = false, letter=false, upW=false, downW=false;

	if (password.length() >= 4)
	{
		//for loop to check if containe spaces / upperLetter / lowerLetter / letter.
		for (int i = 0; i < password.length(); i++)
		{
			//spaces
			if (isspace(password.at(i)))
			{
				space = true;
			}
			//lower
			if (islower(password.at(i)))
			{
				downW = true;
			}
			//upper
			if (isupper(password.at(i)))
			{
				upW = true;
			}
			//letter
			if (isdigit(password.at(i)))
			{
				letter = true;
			}
		}
	}

	if (space == false && upW == true && downW == true &&letter==true)
	{
		return true;
	}
	return false;
}

bool Validator::isUserNameValid(string userName)
{
	bool space = false , letter=false;
	if (!userName.empty())
	{
		//for loop to check if containe spaces and begin with letter.
		for (int i = 0; i < userName.length(); i++)
		{
			//spaces
			if (isspace(userName.at(i)))
			{
				space = true;
			}
		}

		if (isalpha(userName.at(0)))
		{
			letter = true;
		}
	}

	if (space == false && letter == true)
	{
		return true;
	}
	return false;
}