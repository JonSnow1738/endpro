#include "Room.h"

Room::Room(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime, User* admin) :_name(roomName)
{
	_id = roomId;
	_maxUsers = maxUsers;
	_questionTime = questionTime;
	_questionNo = questionsNo;
	_admin = admin;

}

void Room::setFull(bool a){
	_full = a;
}

User* Room::getAdmin(){
	return _admin;
}
bool Room::joinRoom(User* user)
{	
	if (_users.size() < _maxUsers)
	{
		_users.push_back(user);
		return true;
	}

	return false;
}

string Room::getUsersListMessage(){
	string users,ret;
	int userAmount;
	userAmount = _users.size();

	ret = "108";
	ret += to_string(userAmount);

	for (unsigned i = 0; i < _users.size(); i++)
	{
		ret += (_users.at(i)->getUserName().length()<10) ? "0" + to_string(_users.at(i)->getUserName().length()) : to_string(_users.at(i)->getUserName().length());

		ret += _users.at(i)->getUserName();
	}

	return ret;
}

int Room::getQuestionsNo(){
	return _questionNo;
}

int Room::getTime(){
	return _questionTime;
}

int Room::getId(){
	return _id;
}

string Room::getName(){
	return _name;
}

void Room::sendMessage(string msg)
{
	sendMessage(NULL,msg);
}
void Room::sendMessage(User* user, string msg)
{
	int i = 0;
	for (i; i < _users.size();i++)
	{
		if (_users.at(i) != user){
			_users.at(i)->send(msg);
		}
	}
}


void Room::leaveRoom(User* user){
	for (int i=0; i < _users.size(); i++)
	{
		if (_users.at(i) == user)
		{
			_users.erase(_users.begin()+i);
			Helper::sendData(_users.at(i)->getSocket(), "1120");
		}
	}

	sendMessage(user,getUsersListMessage());
}

int Room::closeRoom(User* user)
{
	if (user != _admin)
	{
		return -1;
	}

	for (int i = 0; _users.size(); i++)
	{
		if (_users.size() == 1){
			break;
		}
		if (_users.at(i) != _admin)
		{
			Helper::sendData(_users.at(i)->getSocket(), "116");
			_users.at(i)->clearRoom();
		}
	}

	return(this->getId());
}

vector<User*> Room::getUsers()
{
	return _users;
}