#include "DataBase.h"

unordered_map<string, vector<string>> _results;


DataBase::DataBase(){
	
	_zErrMsg = 0;
	

	_rc = sqlite3_open("trivia.db", &_db);

	if (_rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(_db));
	}
	else {
		fprintf(stderr, "Opened database successfully\n");
	}
}

DataBase::~DataBase(){
	sqlite3_close(_db);
}

DataBase& DataBase::operator=(const DataBase& other)
{
	_db = other._db;
	_rc = other._rc;
	_zErrMsg = other._zErrMsg;
	return *this;
}

bool DataBase::isUserExists(string name){

	string commend = "select count(1) from t_users where username = \"" + name+"\"";
	const char *sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		if (_results.find("count(1)")->second.at(0) == "1")
		{
			return true;
		}
	}

	return false;
}

int DataBase::callbackCount(void* notUsed, int argc, char** argv, char** azCol){
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = _results.find(azCol[i]);
		if (it != _results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			_results.insert(p);
		}
	}

	return 0;
}

bool DataBase::addNewUser(string user, string pass, string mail)
{
	string commend = "INSERT INTO t_users(username, password, email) VALUES (\""+user+"\", \""+pass+"\", \""+ mail+"\");";
	const char *sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		return true;
	}

	return false;
}

bool DataBase::isUserAndPassMatch(string user, string pass)
{
	string commend = "select username from t_users where username =\"" + user + "\" and password =\"" + pass + "\"";
	const char *sqlCommend = commend.c_str();
	_results.clear();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		if (_results.size() != 0){
			if (_results.find("username")->second.at(0) == user)
			{
				return true;
			}
		}
	}

	return false;
}

int DataBase::insertNewGame(){
	string commend = "INSERT INTO t_games(status, start_time, end_time) VALUES (0 ,\"NOW\" ,NULL);";
	const char *sqlCommend = commend.c_str();


	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{

		commend = "select game_id from t_games where start_time = \"NOW\" ";
		const char *sqlCommend = commend.c_str();

		_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);
		if (_rc == SQLITE_OK)
		{
			int i = stoi(_results.find("game_id")->second.at(_results.find("game_id")->second.size() -1 ));
			return i;
		}
	}
	return 0;
}

bool DataBase::updateGameStatus(int id)
{
	string commend = "update t_games set status = 1 and end_time = \"NOW\" ";
	const char *sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		return true;
	}

	return false;
}

bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	int correct = 0;
	int answers = 0;
	const char *sqlCommend;
	string commend;

	if (isCorrect == true)
	{
		correct = 1;
	}

	commend = "INSERT INTO t_players_answers(game_id, username, question_id,player_answer ,is_correct, answer_time) VALUES (" + to_string(gameId) + ", \"" + username + "\", " + to_string(questionId) + ",\"" + answer + "\"," + to_string(correct) + "," + to_string(answerTime) + ");";
	sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);


	if (_rc == SQLITE_OK)
	{
		_results.clear();
		if (isCorrect == true){
			commend = "select numberOfRightAnswers from t_users where username = \"" + username + "\"";
			sqlCommend = commend.c_str();

			_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);
			if (_rc == SQLITE_OK)
			{
				answers = stoi(_results.find("numberOfRightAnswers")->second.at(0));
			}

			commend = "update t_users set numberOfRightAnswers = " + to_string(answers + 1) + " where username = \"" + username + "\"";
			sqlCommend = commend.c_str();

			_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);
		}
		else{
			commend = "select numberOfWrongAnswers from t_users where username = \"" + username + "\"";//problem here
			sqlCommend = commend.c_str();

			_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

			if (_rc == SQLITE_OK)
			{
				answers = stoi(_results.find("numberOfWrongAnswers")->second.at(0));
			}

			commend = "update t_users set numberOfWrongAnswers = " + to_string(answers + 1) + " where username = \"" + username + "\"";
			sqlCommend = commend.c_str();

			_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);
		}

		return true;
	}

	return false;
}

vector<Question*> DataBase::initQuestion(int num)
{
	questionAm = num;
	vector<Question*> questions;
	srand(time(NULL));
	int id;
	vector<int> vec;
	string Id;
	for (int i = 0; i < num; i++)
	{
		id = rand() % 10 + 1;
		if (find(vec.begin(), vec.end(), id) == vec.end()){
			vec.push_back(id);
			Id = to_string(id);
			string commend = "select * from t_questions where question_id = \"" + Id+"\"";
			const char *sqlCommend = commend.c_str();
			_results.clear();

			_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

			if (_rc == SQLITE_OK)
			{
				int id_quest = stoi(_results.find("question_id")->second.at(0));
				string question = _results.find("question")->second.at(0);
				string correct_ans = _results.find("correct_ans")->second.at(0);
				string ans2 = _results.find("ans2")->second.at(0);
				string ans3 = _results.find("ans3")->second.at(0);
				string ans4 = _results.find("ans4")->second.at(0);

				Question *quest = new Question(id_quest, question, correct_ans, ans2, ans3, ans4);
				sleep_until(system_clock::now() + seconds(1));
				questions.push_back(quest);
			}
		}
		else
		{
			i--;
		}
	}

	return questions;
}


vector<string> DataBase::getBestScores()
{
	int i,j;
	int rightanswers;
	vector<int> bestS;
	vector<string> scores;
	map < string, int > mosa;
	vector<string> usersList;
	string commend = "select username from t_users";
	const char *sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		for (i = 0; i < _results.find("username")->second.size(); i++)
		{
			usersList.push_back(_results.find("username")->second.at(i));
		}
	}

	for (i = 0; i < usersList.size(); i++)
	{
		commend = "select numberOfRightAnswers from t_users where username = \"" + usersList[i] + "\"";
		sqlCommend = commend.c_str();
		_results.clear();
		_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

		if (_rc == SQLITE_OK)
		{
			if (!_results.empty())
			{
				rightanswers = stoi(_results.find("numberOfRightAnswers")->second.at(0));
				mosa.insert(pair<string, int>(usersList[i], rightanswers));
			}
		}
	}

	std::map<string, int>::iterator it;
	std::map<string, int>::iterator it2;



	for (it = mosa.begin(); it != mosa.end(); it++)
	{
		bestS.push_back(it->second);
	}
	bestS = bubbleSort(bestS);

	for (j = 0; j < 3; j++)
	{
		for (auto &i : mosa) {
			if (i.second == bestS.at(j)) {
				scores.push_back(i.first);
				scores.push_back(to_string(bestS.at(j)));
			}
		}
	}
	return scores;
}

vector<string> DataBase::getPersonalStatus(string user){
	int i = 0;
	string commend = "";
	const char *sqlCommend;
	vector<string> status;
	_results.clear();
	int right, wrong;
	float average;
	commend = "select numberOfGames from t_users where username = \"" + user + "\"";
	sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		status.push_back(_results.find("numberOfGames")->second.at(0));
	}

	commend = "select numberOfRightAnswers from t_users where username = \"" + user + "\"";
	sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		right = stoi(_results.find("numberOfRightAnswers")->second.at(0));
		status.push_back(to_string(right));
	}

	commend = "select numberOfWrongAnswers from t_users where username = \"" + user + "\"";
	sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		wrong = stoi(_results.find("numberOfWrongAnswers")->second.at(0));
		status.push_back(to_string(wrong));
	}

	commend = "select avregeTime from t_users where username = \"" + user + "\"";
	sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		average = stof(_results.find("avregeTime")->second.at(0));
		if (right == 0 || wrong == 0)
		{
			average = 0;
		}
		else
		{
			average = average / (right + wrong);
		}
		status.push_back(to_string(average));
	}

	return status;
}

vector<int> DataBase::bubbleSort(vector<int> values)
{
	for (int i = 0; i < values.size(); i++)
		for (int j = 0; j < values.size() - i -1; j++)
			if (values[j] < values[j + 1])
				swap(values[j], values[j + 1]);

	return values;
}

void DataBase::gameAm(vector<User*> users)
{
	int i = 0; 
	string commend = "";
	const char *sqlCommend ;
	int numberofgames = 0;
	string user = "";

	for (i; i < users.size(); i++)
	{
		user = users.at(i)->getUserName();
		commend = "select numberOfGames from t_users where username = \"" + user + "\"";//problem here
		sqlCommend = commend.c_str();

		_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

		if (_rc == SQLITE_OK)
		{
			numberofgames = stoi(_results.find("numberOfGames")->second.at(0));

			commend = "update t_users set numberOfGames = " + to_string(numberofgames + 1) + " where username = \"" + users.at(i)->getUserName() + "\"";
			sqlCommend = commend.c_str();
			_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);
		}
		
	}
}

void DataBase::updateAveTime(User* user, int ansTime)
{
	int i = 0;
	string commend = "";
	const char *sqlCommend;
	int time = 0;
	string userName = "";

	
	userName = user->getUserName();
	commend = "select avregeTime from t_users where username = \"" + userName + "\"";//problem here
	sqlCommend = commend.c_str();

	_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);

	if (_rc == SQLITE_OK)
	{
		time = stoi(_results.find("avregeTime")->second.at(0));

		commend = "update t_users set avregeTime = " + to_string(time + ansTime) + " where username = \"" + userName + "\"";
		sqlCommend = commend.c_str();
		_rc = sqlite3_exec(_db, sqlCommend, callbackCount, 0, &_zErrMsg);
	}

}